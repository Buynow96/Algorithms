package cn.zhaoyuening.algorithms.question;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Zhao on 2017/4/12.
 *
 * 一种双核CPU的两个核能够同时的处理任务，现在有n个已知数据量的任务需要交给CPU处理，假设已知CPU的每个核1秒可以处理1kb，每个核同时只能处理一项任务。n个任务可以按照任意顺序放入CPU进行处理，现在需要设计一个方案让CPU处理完这批任务所需的时间最少，求这个最小的时间。
 输入描述:
 输入包括两行：
 第一行为整数n(1 ≤ n ≤ 50)
 第二行为n个整数length[i](1024 ≤ length[i] ≤ 4194304)，表示每个任务的长度为length[i]kb，每个数均为1024的倍数。


 输出描述:
 输出一个整数，表示最少需要处理的时间

 输入例子:
 5
 3072 3072 7168 3072 1024

 输出例子:
 9216
 */
public class Q1 {
    public static int compute(int[] array) {
        int result = Integer.MAX_VALUE;
        int sum=0;
        int avg=0;
        int length = array.length;

        for (int i :
                array) {
            sum+=i;
        }

        avg=sum/2;

        Arrays.sort(array);
        for(int i=1;i<length;i++){
            int avg2 = avg/i;
            Node[] arr = new Node[length];
            for (int j=0;j<length;j++){
                arr[j] = new Node();
                arr[j].b=array[j]-avg2;
                arr[j].b = arr[j].b >= 0 ? arr[j].b : -arr[j].b;
                arr[j].a = array[j];
            }
            Arrays.sort(arr);
            int r2=0;
            for (int j=0;j<i;j++){
                r2+=arr[j].a;
            }
            if (r2 < avg) {
                r2=sum-r2;
            }
            if (r2<result){
                result=r2;
            }
            System.out.println();
        }


        return result;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        sc.nextLine();
        String s = sc.nextLine();
        String[] sCount = s.split(" ");
        int[] array = new int[sCount.length];
        for(int j=0;j<sCount.length;j++){
            array[j] = Integer.parseInt(sCount[j]);
        }

        //1024 3072 3072 3072 7168
        int result = compute(array);
        System.out.println(result);

    }

    private static class Node implements Comparable<Node>{
        int a;
        int b;

        @Override
        public int compareTo(Node o) {
            return this.b-o.b;
        }
    }
}
