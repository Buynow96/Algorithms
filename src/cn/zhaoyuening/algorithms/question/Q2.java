package cn.zhaoyuening.algorithms.question;

import cn.zhaoyuening.algorithms.divideandconquer.QueryMaxSubArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhao on 2017/4/13.
 * 终于到周末啦！小易走在市区的街道上准备找朋友聚会，突然服务器发来警报,小易需要立即回公司修复这个紧急bug。假设市区是一个无限大的区域，每条街道假设坐标是(X，Y)，小易当前在(0，0)街道，办公室在(gx,gy)街道上。小易周围有多个出租车打车点，小易赶去办公室有两种选择，一种就是走路去公司，另外一种就是走到一个出租车打车点，然后从打车点的位置坐出租车去公司。每次移动到相邻的街道(横向或者纵向)走路将会花费walkTime时间，打车将花费taxiTime时间。小易需要尽快赶到公司去，现在小易想知道他最快需要花费多少时间去公司。
 输入描述:
 输入数据包括五行:

 第一行为周围出租车打车点的个数n(1 ≤ n ≤ 50)

 第二行为每个出租车打车点的横坐标tX[i] (-10000 ≤ tX[i] ≤ 10000)

 第三行为每个出租车打车点的纵坐标tY[i] (-10000 ≤ tY[i] ≤ 10000)

 第四行为办公室坐标gx,gy(-10000 ≤ gx,gy ≤ 10000),以空格分隔

 第五行为走路时间walkTime(1 ≤ walkTime ≤ 1000)和taxiTime(1 ≤ taxiTime ≤ 1000),以空格分隔


 输出描述:
 输出一个整数表示，小易最快能赶到办公室的时间

 输入例子:
 2
 -2 -2
 0 -2
 -4 -2
 15 3

 输出例子:
 42
 */
public class Q2 {

    private static int compute(List<Node> taxiNodeList,Node officeNode,int walkTime,int taxiTime){
        //最短时间结果
        int result = Integer.MAX_VALUE;
        //创建自己的位置
        Node self = new Node(0,0);
        self.x=0;
        self.y=0;

        //计算走路时间
        int walkResult = walkTime * (self.getDistance(officeNode));
        int taxiResult = 0;
        result = walkResult;
        //计算出租车时间
        for (Node taxiNode :
                taxiNodeList) {
            walkResult = walkTime * (self.getDistance(taxiNode));
            taxiResult = walkResult + taxiTime * (taxiNode.getDistance(officeNode));
            if (taxiResult< result){
                result = taxiResult;
            }
        }
        return result;
    }
    public static void main(String[] args) {
        List<Node> taxiNodeList = new ArrayList<>();
        taxiNodeList.add(new Node(-2,0));
        taxiNodeList.add(new Node(-2,-2));
        int result = compute(taxiNodeList, new Node(-4, -2), 15, 3);
        System.out.println(result);
    }

    public static class Node {
        public Node(){}
        public Node(int x,int y){
            this.x = x;
            this.y = y;
        }
        int x;
        int y;
        public int getDistance(Node node){
            int distance = 0;
            int xDi = this.x-node.x;
            xDi = xDi>=0?xDi:-xDi;
            int yDi = this.y-node.y;
            yDi = yDi>=0?yDi:-yDi;
            distance = xDi+yDi;
            return distance;
        }
    }
}
