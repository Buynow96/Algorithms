package cn.zhaoyuening.algorithms.question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhao on 2017/4/13.
 * 在幼儿园有n个小朋友排列为一个队伍，从左到右一个挨着一个编号为(0~n-1)。其中有一些是男生，有一些是女生，男生用'B'表示，女生用'G'表示。小朋友们都很顽皮，当一个男生挨着的是女生的时候就会发生矛盾。作为幼儿园的老师，你需要让男生挨着女生或者女生挨着男生的情况最少。你只能在原队形上进行调整，每次调整只能让相邻的两个小朋友交换位置，现在需要尽快完成队伍调整，你需要计算出最少需要调整多少次可以让上述情况最少。例如：
 GGBBG -> GGBGB -> GGGBB
 这样就使之前的两处男女相邻变为一处相邻，需要调整队形2次
 输入描述:
 输入数据包括一个长度为n且只包含G和B的字符串.n不超过50.


 输出描述:
 输出一个整数，表示最少需要的调整队伍的次数

 输入例子:
 GGBBG

 输出例子:
 2
 */
public class Q3 {
    private static int compute(int[] arr){
        int stepCount = 0;
        //最靠右的-1下标
        int lastRightIndex;
        //1 位置的下标索引
        List<Integer> indexArr1 = new ArrayList<>();
        //-1 位置的下标索引
        List<Integer> indexArr2 = new ArrayList<>();
        //记录下标
        for (int i=arr.length-1;i>=0;i--){
            if (arr[i]==1){
                indexArr1.add(i);
            }else {
                indexArr2.add(i);
            }
        }
        int result1=0;
        int result2=0;

        //计算 左边放-1右边放 1
        for (int i=0;i<indexArr2.size();i++){
            for (int j=0;j<indexArr1.size();j++){
                if (indexArr1.get(j)>indexArr2.get(i)){
                    result1++;
                }else {
                    result2++;
                }
            }
        }
        stepCount = result1>result2?result2:result1;
        //计算 左边放 1右边放-1
        return stepCount;
    }
    public static void main(String[] args) {
        int result = compute(new int[]{1, 1, -1, -1, 1});
        System.out.println(result);
    }
}
