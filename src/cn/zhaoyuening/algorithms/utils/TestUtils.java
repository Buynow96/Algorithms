package cn.zhaoyuening.algorithms.utils;

import java.util.Random;

/**
 * Created by Zhao on 2017/3/26.
 */
public class TestUtils {
    public static Integer[] getArr(int length, int max,boolean hasNegative ){

        Random random = new Random();
        Integer[] arr = new Integer[length];
        if (hasNegative){
            max*=2;
        }
        for(int i=0;i<length;i++) {

            int count = random.nextInt(max);
            if (hasNegative){
                arr[i]=count-(max/2);
            }else{
                arr[i]=count;
            }
        }
        return arr;
    }
}
