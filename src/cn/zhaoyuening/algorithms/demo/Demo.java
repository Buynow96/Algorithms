package cn.zhaoyuening.algorithms.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Buynow on 2017/7/15.
 */
public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int result = 0;
        int[] tasks = new int[n];

        sc.nextLine();
        String[] strings = sc.nextLine().split(" ");
        // init tasks arr
        for (int i=0;i<strings.length;i++) {
            tasks[i] = Integer.parseInt(strings[i]);
        }

        //sort the tasks
        Arrays.sort(tasks);

        List<Integer> cpu1List = new ArrayList<>();
        int cpu1Time = 0;
        List<Integer> cpu2List = new ArrayList<>();
        int cpu2Time = 0;
        int amount = 0;
        for (int i=0;i<n;i++) {
            if (i % 2 == 0) {
                cpu2List.add(tasks[i]);
                cpu2Time += tasks[i];
            }else{
                cpu1List.add(tasks[i]);
                cpu1Time += tasks[i];
            }
        }
        if (cpu1List.size() == 0 || cpu2List.size() == 0) {
            result = Math.abs(cpu1Time - cpu2Time);
            System.out.println(result);
            return;
        }
        int cpu1MinIndex = cpu1List.size() - 1;
        int cpu1MinTime = cpu1List.get(cpu1MinIndex);
        int cpu2MinIndex = cpu2List.size() - 1;
        int cpu2MinTime = cpu2List.get(cpu2MinIndex);

        while (true) {
            //时间差
            int x = cpu1Time-cpu2MinIndex;
            if (x>0) {
                if (cpu1MinTime > x) {
                    break;
                }
                cpu2List.add(cpu1List.get(cpu1MinIndex));
                cpu1List.remove(cpu1MinIndex);
                

            }else{
                x = Math.abs(x);
                if (cpu2MinTime > x) {
                    break;
                }
            }
        }



    }
}
