package cn.zhaoyuening.algorithms.demo;

import java.util.*;

/**
 * Created by Buynow on 2017/8/8.
 */
public class Main2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
        String[] split = sc.nextLine().trim().split(" ");
        byte[] datas = new byte[split.length];
        for (int i=0;i<split.length;i++) {
            datas[i] = Byte.parseByte(split[i]);
        }

        helper(datas);
    }

    private static void helper(byte[] datas) {
        long counter = 0;
        byte tmp = datas[0];
        for (byte b:datas){
            if (b != tmp) {
                tmp = b;
                counter++;
            }
        }
        if (datas[0] == 1) {
            counter++;
        }
        System.out.println(counter%2==0?"Bob":"Alice");
    }

    public boolean isSuccess(byte[] datas) {
        for (byte b :
                datas) {
            if (b == 1) {
                return false;
            }
        }
        return true;
    }
}
