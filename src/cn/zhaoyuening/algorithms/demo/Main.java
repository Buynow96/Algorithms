package cn.zhaoyuening.algorithms.demo;


import java.util.*;


/**
 * Created by Buynow on 2017/7/15.
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long[] datas = new long[n];
        for (int i=0;i<n;i++) {
            datas[n] = sc.nextLong()%7;
        }
        helper(datas);
    }

    private static void helper(long[] datas) {
        if (datas.length <= 1) {
            System.out.println(0);
            return;
        }
        StringBuilder sb = new StringBuilder();
        long counter = 0;
        for (int i=0;i<datas.length;i++) {

            for (int j=0;j<datas.length;j++) {
                if (i == j) {
                    continue;
                }
                sb.delete(0, sb.length());
                sb.append(datas[i]);
                sb.append(datas[j]);
                if(Integer.parseInt(sb.toString())%7==0){
                    counter++;
                }
            }
        }
        System.out.println(counter);
    }


}
