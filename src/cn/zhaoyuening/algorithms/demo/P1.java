package cn.zhaoyuening.algorithms.demo;

import java.util.Scanner;

/**
 * Created by Buynow on 2017/7/29.
 */
public class P1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        System.out.print(n+"=");
        helper(n,2);
    }
    //最小质数x
    public static void helper(int n, int x) {
        while (x <= n) {
            if (x == n) {
                System.out.println(n);
                return;
            } else if (n > x && n % x != 0) {
                x++;
                helper(n,x);
                return;
            } else if (n > x && n % x == 0) {
                System.out.print(x + "*");
                n /= x;
                helper(n,x);
                return;
            }
        }
    }
}
