package cn.zhaoyuening.algorithms.demo;

import java.util.Scanner;

/**
 * Created by Buynow on 2017/8/5.
 */
public class P2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        String[] split = s.split(" ");

        long n = Long.parseLong(split[0]);
        long k = Long.parseLong(split[1]);
        long d = n;
        long log = log2(n);
        int j = 0;
        for (int i=0;i<=log;i++) {
            if ((n >> i)%2==0) {
                n += ((k >> j) % 2)*Math.pow(2,i);
                j++;
            }
        }
        n ^= d;
        n |= k>>j<<(log+1);
        System.out.println(n);
    }

    public static long log2(long l) {
        return (long) (Math.log(l) / Math.log(2));
    }
}
