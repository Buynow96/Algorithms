package cn.zhaoyuening.algorithms.stack;

import java.util.Stack;

/**
 * Created by Buynow on 2017/8/6.
 */
public class SortStack {
    public static void sortStack(Stack<Integer> stack) {
        Stack<Integer> sortStack = new Stack<>();
        while (!stack.isEmpty()) {
            addValueBySorted(stack.pop(),sortStack);
        }
        while (!sortStack.isEmpty()) {
            stack.add(sortStack.pop());
        }
    }

    public static void addValueBySorted(Integer val, Stack<Integer> stack) {
        if (stack.isEmpty()||stack.peek()<=val) {
            stack.add(val);
            return ;
        }
        int i = stack.pop();
        addValueBySorted(val,stack);
        stack.add(i);
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.add(3);
        stack.add(1);
        stack.add(2);
        stack.add(4);
        System.out.println(stack);
        sortStack(stack);
        System.out.println(stack);
    }
}
