package cn.zhaoyuening.algorithms.stack;

import java.util.Stack;

/**
 * Created by Buynow on 2017/8/6.
 */
public class InvertStackOperation {
    public static Object getLastStackElementAndRemove(Stack stack) {
        Object element = stack.pop();
        Object result = element;
        if (!stack.isEmpty()) {
            result = getLastStackElementAndRemove(stack);
            stack.push(element);
        }

        return result;
    }

    public static void invertStack(Stack stack) {
        if (stack.isEmpty()) {
            return;
        }
        Object element = getLastStackElementAndRemove(stack);
        invertStack(stack);
        stack.push(element);
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        System.out.println(stack);
        invertStack(stack);
        System.out.println(stack);
    }
}
