package cn.zhaoyuening.algorithms.test;

import cn.zhaoyuening.algorithms.sort.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Created by Zhao on 2017/3/25.
 */
public class SortTest {
    @Test
    public void testMergeSort() {
        sortTest(getArr(100000,100),new MergeSort());
        sortTest(getArr(100000,100),new BubblingSort());

    }

    @Test
    public void test2() throws Exception {
        Integer[] arr = getArr(10000,10000);
        System.out.println(Arrays.toString(arr));
        Sort<Integer> mergeSort = new MergeSort<>();
        long time = SortExcuteor.excute(mergeSort, arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(time);
    }

    private Integer[] getArr(int length, int max){
        Random random = new Random();
        Integer[] arr = new Integer[length];
        for(int i=0;i<length;i++) {
            int count = random.nextInt(max);
            arr[i]=count;
        }
        return arr;
    }

    @Test
    public void name() throws Exception {
        Integer[] arr = getArr(6, 10);
        System.out.println(Arrays.toString(arr));

    }

    /**
     * 测试 sort 并显示运算时间
     * @param integers
     * @param sort
     */
    public void sortTest(Integer[] integers,Sort sort) {
        Integer[] arr = integers;
        long time = SortExcuteor.excute(sort, arr);
        System.out.println(time);
    }
}
