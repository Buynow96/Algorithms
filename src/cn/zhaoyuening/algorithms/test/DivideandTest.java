package cn.zhaoyuening.algorithms.test;

import cn.zhaoyuening.algorithms.divideandconquer.QueryMaxSubArray;
import cn.zhaoyuening.algorithms.utils.TestUtils;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by Zhao on 2017/3/26.
 * 分治算法
 */
public class DivideandTest {
    @Test
    public void testQueryMaxSubArray() {
        Integer[] arr = TestUtils.getArr(100, 1000, true);
        System.out.println(Arrays.toString(arr));
        long sTime = System.nanoTime();
        QueryMaxSubArray.Result result = QueryMaxSubArray.getMaxSubArray(arr, 0, arr.length - 1);
        long eTime = System.nanoTime();
        System.out.println(result.getLow());
        System.out.println(result.getHigh());
        System.out.println(result.getSum());
        System.out.println(eTime-sTime);
    }
}
