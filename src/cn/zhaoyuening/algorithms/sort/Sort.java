package cn.zhaoyuening.algorithms.sort;

/**
 * Created by Zhao on 2017/3/25.
 */
public interface Sort<T> {
    Comparable<T>[] sort(Comparable<T>[] comparable);

}
