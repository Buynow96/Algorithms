package cn.zhaoyuening.algorithms.sort;

/**
 * Created by Zhao on 2017/3/25.
 */
public class SortExcuteor{
    public static long excute(Sort sort,Comparable[] comparables){
        long startTime = System.nanoTime();//System.currentTimeMillis();
        sort.sort(comparables);
        long endTime = System.nanoTime();//System.currentTimeMillis();

        return endTime-startTime;
    }

}
