package cn.zhaoyuening.algorithms.sort;

/**
 * Created by Zhao on 2017/3/25.
 * 冒泡排序
 */
public class BubblingSort<T> implements Sort<T> {
    @Override
    public Comparable<T>[] sort(Comparable<T>[] comparables) {
        for (int i=0;i<comparables.length;i++){
            for (int j=i+1;j<comparables.length;j++){
                if (comparables[i].compareTo((T) comparables[j])>0){
                    Comparable<T> tmp = comparables[i];
                    comparables[i] = comparables[j];
                    comparables[j] = tmp;
                }
            }
        }
        return comparables;
    }
}
