package cn.zhaoyuening.algorithms.sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Zhao on 2017/3/25.
 * java list sort
 */
public class JavaSort<T> implements Sort<T> {

    @Override
    public Comparable<T>[] sort(Comparable<T>[] comparable) {
        List<Comparable<T>> list = Arrays.asList(comparable);
        list.sort(new Comparator<Comparable<T>>() {
            @Override
            public int compare(Comparable<T> o1, Comparable<T> o2) {
                return o1.compareTo((T) o2);
            }
        });
        Comparable<T>[] array = (Comparable<T>[]) list.toArray();
        return array;
    }
}
