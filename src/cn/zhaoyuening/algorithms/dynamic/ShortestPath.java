package cn.zhaoyuening.algorithms.dynamic;

/**
 * Created by Buynow on 2017/8/14.
 */
public class ShortestPath {
    public static void helper(int[][] pathArr) {
        int[][] dp = new int[pathArr.length][pathArr[0].length];
        dp[0][0] = pathArr[0][0];
        for (int i=1;i<pathArr[0].length;i++) {
            dp[0][i] = pathArr[0][i]+dp[0][i-1];
        }

        for (int i=1;i<pathArr.length;i++) {
            dp[i][0] = pathArr[i][0]+dp[i-1][0];
        }

        int tmpMin = 0;
        for (int i=1;i<pathArr.length;i++) {
            for (int j=1;j<pathArr[0].length;j++) {
                tmpMin = dp[i - 1][j];
                tmpMin = Math.min(tmpMin, dp[i][j - 1]);
                dp[i][j] = tmpMin + pathArr[i][j];
            }
        }

        System.out.println(dp[pathArr.length-1][pathArr[0].length-1]);
    }

    public static void main(String[] args) {
        int[][] pathArr = new int[][]{
                {1, 3, 5, 9},
                {8, 1, 3, 4},
                {5, 0, 6, 1},
                {8, 8, 4, 0}
        };
        helper(pathArr);
    }
}
