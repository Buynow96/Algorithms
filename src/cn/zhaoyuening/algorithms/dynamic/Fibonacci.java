package cn.zhaoyuening.algorithms.dynamic;

/**
 * Created by Buynow on 2017/8/14.
 */
public class Fibonacci {

    public static void helper(int n) {
        int a = 1;
        int b = 1;
        if (n <= 2) {
            System.out.println(a);
            return;
        }
        int tmp = 0;
        for (int i=1;i<=n;i++) {
            if (i == n) {
                System.out.println(a+b);
                return;
            }
            tmp = a;
            a = b;
            a = b;
            a += b;
        }
    }

    public static void main(String[] args) {
        helper(3);
    }
}
