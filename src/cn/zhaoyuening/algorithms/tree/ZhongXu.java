package cn.zhaoyuening.algorithms.tree;

import cn.zhaoyuening.algorithms.tree.core.TreeNode;

/**
 * Created by Buynow on 2017/7/16.
 */
public class ZhongXu {
    public static void main(String[] args) {
        zhongxu(DemoTree.getDemoTree());
    }
    public static void zhongxu(TreeNode treeNode) {
        if (treeNode == null) {
            return ;
        }
        zhongxu(treeNode.getLeftNode());
        System.out.println(treeNode.getData());
        zhongxu(treeNode.getRightNode());
    }


}
