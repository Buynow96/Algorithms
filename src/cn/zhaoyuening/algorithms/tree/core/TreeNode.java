package cn.zhaoyuening.algorithms.tree.core;

/**
 * Created by Buynow on 2017/7/15.
 */
public class TreeNode<T> {
    private TreeNode<T> leftNode;
    private TreeNode<T> rightNode;
    private T data;

    public TreeNode<T> getLeftNode() {
        return leftNode;
    }

    public TreeNode setLeftNode(TreeNode<T> leftNode) {
        this.leftNode = leftNode;
        return this;
    }

    public TreeNode<T> getRightNode() {
        return rightNode;
    }

    public TreeNode setRightNode(TreeNode<T> rightNode) {
        this.rightNode = rightNode;
        return this;
    }

    public T getData() {
        return data;
    }

    public TreeNode setData(T data) {
        this.data = data;
        return this;
    }
}
