package cn.zhaoyuening.algorithms.tree;

import cn.zhaoyuening.algorithms.tree.core.TreeNode;

/**
 * Created by Buynow on 2017/7/15.
 */
public class DemoTree {
    public static TreeNode<Integer> getDemoTree(){
        TreeNode<Integer> node1 = new TreeNode<>();
        TreeNode<Integer> node2 = new TreeNode<>();
        TreeNode<Integer> node3 = new TreeNode<>();
        TreeNode<Integer> node4 = new TreeNode<>();
        TreeNode<Integer> node5 = new TreeNode<>();
        TreeNode<Integer> node6 = new TreeNode<>();
        TreeNode<Integer> node7 = new TreeNode<>();
        node1.setData(1).setLeftNode(node2).setRightNode(node3);
        node2.setData(2).setLeftNode(node4).setRightNode(node5);
        node3.setData(3).setLeftNode(node6).setRightNode(node7);
        node4.setData(4);
        node5.setData(5);
        node6.setData(6);
        node7.setData(7);

        return node1;
    }
}
