package cn.zhaoyuening.algorithms.tree;

import java.util.Arrays;

/**
 * Created by Buynow on 2017/8/6.
 */
public class MaxTree  {
    public static void keepMaxTree(int[] arr, int i) {
        if (i == 0) {
            return;
        }
        int parent = (i+1)/2-1;
        if (arr[parent] < arr[i]) {
            exchange(arr,i,parent);
        }
    }

    public static void buildMaxTree(int[] arr) {
        for (int i=1;i<arr.length;i++) {
            keepMaxTree(arr,i);
        }
    }

    public static void exchange(int[] arr, int a, int b) {
        int tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{3, 4, 5, 1, 2};
        buildMaxTree(arr);
        System.out.println(Arrays.toString(arr));
    }
}
