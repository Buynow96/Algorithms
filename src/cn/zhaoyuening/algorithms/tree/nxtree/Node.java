package cn.zhaoyuening.algorithms.tree.nxtree;

/**
 * Created by Buynow on 2017/6/19.
 */
public class Node<T> {
    private T t;
    private int size;
    private boolean isDir;
    private Node<T> firstChild;
    private Node<T> nextSibling;

    public T getT() {
        return t;
    }


    public void setT(T t) {
        this.t = t;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Node<T> getFirstChild() {
        return firstChild;
    }

    public void setFirstChild(Node<T> firstChild) {
        this.firstChild = firstChild;
    }

    public Node<T> getNextSibling() {
        return nextSibling;
    }

    public void setNextSibling(Node<T> nextSibling) {
        this.nextSibling = nextSibling;
    }

    public boolean isDir() {
        return isDir;
    }

    public void setDir(boolean dir) {
        isDir = dir;
    }
}
