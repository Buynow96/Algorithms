package cn.zhaoyuening.algorithms.bishi.pdd2017;

/**
 * Created by Buynow on 2017/8/16.
 */
public class Main {
    public static void helper(int[] arr) {
        int maxIndexA = 0;
        int maxIndexB = 0;
        for (int i=1;i<arr.length;i++) {
            if (Math.abs(arr[i]) > Math.abs(arr[maxIndexB])) {
                if (Math.abs(arr[i]) > Math.abs(arr[maxIndexA])) {
                    maxIndexB = maxIndexA;
                    maxIndexA = i;
                }else{
                    maxIndexB = i;
                }
            }
        }

        int max = arr[maxIndexA] * arr[maxIndexB];
        int t = 0;
        if (max > 0) {
            for (int i=0;i<arr.length;i++) {
                if (i == maxIndexA || i == maxIndexB) {
                    continue;
                }
                if (arr[i] > 0 && arr[i] > t) {
                    t = arr[i];
                }
            }
            System.out.println(max*t);
            return;
        }else{
            for (int i=0;i<arr.length;i++) {
                if (i == maxIndexA || i == maxIndexB) {
                    continue;
                }
            }
        }
    }
    public static void main(String[] args) {

    }
}
