package cn.zhaoyuening.algorithms.bishi.wangyi2018;

/**
 * Created by Buynow on 2017/8/16.
 */
public class Main8 {
    public static final long MOD = 1000000007;
    public static void helper(int k,int n) {
        int[][] dp = new int[k+1][n+1];
        //当possibleArr[i][j] 当最后一个数字为i时 倒数第二个数组能为j时 1 不能为0
        int[][] possibleArr = new int[k+1][k+1];
        for (int i=1;i<=k;i++) {
            for (int j=1;j<=k;j++) {
                if (j <= i) {
                    possibleArr[i][j] = 1;
                } else if (j % i != 0) {
                    possibleArr[i][j] = 1;
                }
            }
        }

        for (int i=1;i<=k;i++) {
            dp[i][1] = 1;
        }
        //长度
        for (int j=2;j<=n;j++) {
            //以该数字结尾
            for (int i=1;i<=k;i++) {
                for (int d=1;d<=k;d++) {
                    if (possibleArr[i][d] == 1) {
                        dp[i][j] += dp[d][j - 1];
                    }
                }
            }
        }
        int sum = 0;
        for (int i=1;i<=k;i++) {
            sum += dp[i][n];
        }
        System.out.println(sum%MOD);
    }

    public static void main(String[] args) {
        helper(2,2);
    }
}
