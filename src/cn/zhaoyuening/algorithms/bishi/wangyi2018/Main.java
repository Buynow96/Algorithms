package cn.zhaoyuening.algorithms.bishi.wangyi2018;

import java.util.*;

/**
 * Created by Buynow on 2017/8/13.
 */
public class Main {
    public static void helper(String s) {
        Map<Character, Integer> map = new HashMap<>();
        char c = ' ';
        //统计字符数量
        for (int i=0;i<s.length();i++) {
            c = s.charAt(i);
            if (map.containsKey(c)) {
                map.put(c, map.get(c)+1);
                continue;
            }
            map.put(c, 1);
        }
        //有两相同字符的 每个字符的个数除2
        List<Integer> doubleCharArr = new ArrayList<>();
        int signleCharCounter = 0;
        int doubleCharCounter = 0;

        Set<Character> characterSet = map.keySet();
        for (Character ch:
                characterSet) {
            Integer count = map.get(ch);
            if (count / 2 > 0) {
                doubleCharArr.add(count / 2);
                doubleCharCounter += count / 2;
            }
            signleCharCounter += count % 2;

        }

        if (signleCharCounter > 2) {
            System.out.println(0);
            return;
        }
        if (signleCharCounter == 2) {
            doubleCharCounter+=1;
        }

        long result = 0;
        result = jiecheng(doubleCharCounter);
        for (Integer n :
                doubleCharArr) {
            result /= jiecheng(n);
        }
        if (signleCharCounter == 2) {
            result/=2;
        }

        System.out.println(result);


    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = "IIIPIIIPIPIPIPIPPPIPIIIIIIPIPIPIPIIIIPP";
//        String s = sc.nextLine();
        helper(s);
    }

    public static long jiecheng(int n) {
        long jiecheng = 1;
        for (int i=n;i>0;i--) {
            jiecheng *= i;
        }
        return jiecheng;
    }
}

/*
*
* 时间限制：1秒
空间限制：32768K
小易有一些彩色的砖块。每种颜色由一个大写字母表示。各个颜色砖块看起来都完全一样。现在有一个给定的字符串s,s中每个字符代表小易的某个砖块的颜色。小易想把他所有的砖块排成一行。如果最多存在一对不同颜色的相邻砖块,那么这行砖块就很漂亮的。请你帮助小易计算有多少种方式将他所有砖块排成漂亮的一行。(如果两种方式所对应的砖块颜色序列是相同的,那么认为这两种方式是一样的。)
例如: s = "ABAB",那么小易有六种排列的结果:
"AABB","ABAB","ABBA","BAAB","BABA","BBAA"
其中只有"AABB"和"BBAA"满足最多只有一对不同颜色的相邻砖块。
输入描述:
输入包括一个字符串s,字符串s的长度length(1 ≤ length ≤ 50),s中的每一个字符都为一个大写字母(A到Z)。


输出描述:
输出一个整数,表示小易可以有多少种方式。

输入例子1:
ABAB

输出例子1:
2*/
