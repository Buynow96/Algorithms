package cn.zhaoyuening.algorithms.bishi.wangyi2018;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Buynow on 2017/8/14.
 */
public class Main4 {
    public static void helper(int[] arr) {
        LinkedList<Integer> list = new LinkedList<>();
        for (int i=0;i<arr.length;i++) {
            if (i % 2 == 0) {
                list.addFirst(arr[i]);
            }else{
                list.addLast(arr[i]);
            }
        }

        if (arr.length % 2 == 0) {
            for (int i=0;i<arr.length;i++) {
                if (i != arr.length - 1) {
                    System.out.print(list.pollLast()+" ");
                }else{
                    System.out.println(list.pollLast());
                }
            }
        }else {
            for (int i=0;i<arr.length;i++) {
                if (i != arr.length - 1) {
                    System.out.print(list.pollFirst()+" ");
                }else{
                    System.out.println(list.pollFirst());
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
        String[] split = sc.nextLine().split(" ");
        int[] arr = new int[split.length];
        for (int i=0;i<split.length;i++) {
            arr[i] = Integer.parseInt(split[i]);
        }

        helper(arr);
    }
}
