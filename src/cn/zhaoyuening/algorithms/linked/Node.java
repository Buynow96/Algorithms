package cn.zhaoyuening.algorithms.linked;

/**
 * Created by Buynow on 2017/8/9.
 */
public class Node<T> {
    private T data;
    private Node<T> next;

    public Node(T data, Node<T> next) {
        this.data = data;
        this.next = next;
    }

    public Node(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public Node setData(T data) {
        this.data = data;
        return this;
    }

    public Node<T> getNext() {
        return next;
    }

    public Node setNext(Node<T> next) {
        this.next = next;
        return this;
    }

    public Node() {
    }
}
