package cn.zhaoyuening.algorithms.test;

import cn.zhaoyuening.algorithms.bishi.wangyi2018.Main;
import org.junit.Test;

import java.util.PriorityQueue;

/**
 * Created by Buynow on 2017/8/14.
 */
public class DemoTest {
    @Test
    public void test1() throws Exception {
        long result = Main.jiecheng(19);
        long a = Main.jiecheng(7);
        long b = Main.jiecheng(12);
        result = result/a;
        result = result/b;
        System.out.println(result);
    }

    @Test
    public void test2() throws Exception {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(2,(o1, o2) -> {
           return o2-o1;
        });

        priorityQueue.add(3);
        priorityQueue.add(54);
        priorityQueue.add(23);
        priorityQueue.add(66);
        System.out.println(priorityQueue.size());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
        System.out.println(priorityQueue.poll());
    }
}
